import matplotlib.pyplot as plt
from enum import Enum
import random
import time
import tqdm
import math
import typing

class Comparator:
    def __init__(self):
        self.comparitions_amount = 0

    '''greater than'''
    def gt(self, a, b):
        self.comparitions_amount += 1
        return a > b


class ShellSort:
    def run(arr, skips, comp: Comparator):
        # print(f"array size: {len(arr)}")
        # print(f"skips : {skips}")

        for skip in skips:
            for i in range(skip, len(arr)):
                temp = arr[i]
                j = i
                while j>=skip and comp.gt(arr[j-skip], temp):
                    arr[j] = arr[j-skip]
                    j-=skip
                arr[j] = temp
        

class Series(Enum):
    N_SQUARED = 0
    N_4_3     = 1
    N_3_2     = 2
    LAZARUS   = 3
    KNUTH     = 4
    TOKUDA   = 5

class Skipper:
    def generateNSquared(size): # dla tablic o size 10 => [5, 2, 1]
        arr = []
        i = size // 2
        while i >= 1:
            arr.append(i)
            i //= 2
        return arr

    def generateN43Element(k):
        if k == 0:
            return 1
        return 4**k + 3 * 2**(k-1) + 1
    
    def generateN32Element(k):
        if k == 0:
            return 1
        return 2**k + 1

    def generateLazarus(size):
        arr = []
        i = 2 * math.floor(size // 4) + 1
        k = 2
        while i > 1:
            arr.append(i)
            i = 2 * math.floor(size // 2**(k+1)) + 1
            k+=1
        arr.append(1)
        return arr

    def generateUnknown(size):
        return [701,301,132,57,23,10,4,1]
    
    def generateKnuth(size):
        arr = []
        k = 1
        i = 0
        while i <= size // 3:
            i = (3**k - 1) // 2
            arr.append(i)
            k+=1
        arr.reverse()
        return arr

    def generatePrattSeries(k):
        pratt_sequence = set()
        p = 1
        while p <= k:
            q = p
            while q <= k:
                pratt_sequence.add(q)
                q *= 3
            p *= 2
        return sorted(pratt_sequence, reverse=True)

    def generateTokudaElement(k):
        k+=1
        return math.ceil(1.8 * 2.25**(k-1) - 0.8)

    def generateSeries(size, generator):
        arr = []
        i = 1
        n = 1
        while i < size:
            arr.append(i)
            i = generator(n)
            n += 1
        arr.reverse()
        return arr

    def generate(size, typ):
        if typ == Series.N_SQUARED:
            return Skipper.generateNSquared(size)
        if typ == Series.N_4_3:
            return Skipper.generateSeries(size, Skipper.generateN43Element)
        if typ == Series.N_3_2:
            return Skipper.generateSeries(size, Skipper.generateN32Element)
        if typ == Series.LAZARUS:
            return Skipper.generateLazarus(size)
        if typ == Series.KNUTH:
            return Skipper.generateKnuth(size)
        if typ == Series.TOKUDA:
            return Skipper.generateSeries(size, Skipper.generateTokudaElement)
        return [1]

class ArrayGenerator:
    def generateSizes():
        return [i for i in range(10_000, 500_001, 10_000)]
        #return [500_001]
        #return [i for i in range(1_000, 50_001, 1_000)]

    def generateRandom(size):
        return [random.randint(0, 100_000) for _ in range(size)]
    
    def generateDesc(size):
        return [i for i in range(size, 1, -1)]
    
class ExperimentRunner:
    def __init__(self, typ):
        self.results = {}
        self.typ = typ
        self.results = {
            "amount_of_elements": [],
            "result_times": [],
            "comparations": []
        }

    def run(self):
        sizes = ArrayGenerator.generateSizes() # [10_000, 20_000, ..., 500_000]
        for size in tqdm.tqdm(sizes):
            comp = Comparator()
            random_numbers = ArrayGenerator.generateRandom(size)
            skips = Skipper.generate(size, self.typ)
            begin_time = time.perf_counter_ns()
            
            ShellSort.run(random_numbers, skips, comp)
            
            end_time = time.perf_counter_ns()
            result_time = end_time - begin_time

            self.results["amount_of_elements"].append(size)
            self.results["result_times"].append(result_time)
            self.results["comparations"].append(comp.comparitions_amount)
        pass


class Ploter:
    def __init__(self):
        plt.style.use("ggplot")
        plt.xlabel("Ilość elementów")
        plt.ylabel("Ilość porównań")
        plt.ticklabel_format(useOffset=False, style='plain')
        plt.grid(visible=True, which='both')
        

    def add_set(self, results, label):
        plt.plot(results["amount_of_elements"], results["comparations"], label=label)

        
    def draw(self):
        plt.legend()
        plt.savefig("all.png", dpi=300, bbox_inches='tight')
        plt.show()


def main():
    experiment1 = ExperimentRunner(Series.N_SQUARED)
    experiment1.run()

    experiment2 = ExperimentRunner(Series.N_4_3)
    experiment2.run()

    experiment3 = ExperimentRunner(Series.N_3_2)
    experiment3.run()

    experiment4 = ExperimentRunner(Series.LAZARUS)
    experiment4.run()

    experiment5 = ExperimentRunner(Series.KNUTH)
    experiment5.run()

    experiment6 = ExperimentRunner(Series.TOKUDA)
    experiment6.run()

    ploter = Ploter()
    ploter.add_set(experiment1.results, "Shell O(n**2)")
    ploter.add_set(experiment2.results, "Sedgewick O(n**(4/3))")
    ploter.add_set(experiment3.results, "Papiernow/Stasiewicz O(n**(3/2))")
    ploter.add_set(experiment4.results, "Lazarus O(n**(3/2))")
    ploter.add_set(experiment5.results, "Knuth")
    ploter.add_set(experiment6.results, "Tokuda, unknown complexity")
    ploter.draw()

    pass

if __name__ == "__main__":
    main()
